import { LightningElement, track, wire} from 'lwc';
import { getPicklistValuesByRecordType } from 'lightning/uiObjectInfoApi';

export default class PicklistValuesByRecordType extends LightningElement {
    @track value;
    @track options;

    @wire(getPicklistValuesByRecordType, { objectApiName: 'Account', recordTypeId: '012B0000000gTUOIA2' }) 
    IndustryPicklistValues({error, data}) {
        if(data) {
            this.options = data.picklistFieldValues.Industry.values;
        }
        else if(error) {
            window.console.log('error =====> '+JSON.stringify(error));
        }
    }

    handleChange(event) {
        this.value = event.target.value;
    }
}